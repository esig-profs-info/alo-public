---
template: item
---

# Public - private (vidéo)
<iframe width="560" height="315" src="https://www.youtube.com/embed/TkrDDSFLyTo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Les modificateurs d'accès

## Définition:
Les modificateurs d'accès sont les mots clés que l'on peut préciser devant des classes, des méthodes et des attributs, ils peuvent être :
- public
- _vide_
- protected
- public

> Ils s'appliquent aux classes, aux méthodes et aux attributs.<br/>
> Ils ne peuvent pas être utilisés pour qualifier des variables locales : seules les variables d'instances et de classes peuvent en profiter.<br/>
> Ils assurent le contrôle des conditions d'héritage, d'accès aux éléments et de modification de données par les autres objets.

## Gérer la visibilité des entités:
De nombreux langages orientés objet introduisent des attributs de visibilité pour réglémenter l'accès aux classes et aux objets, aux méthodes et aux données.<br/>
Il existe 3 modificateurs qui peuvent être utilisés pour définir les attributs de visibilité des entités (classes, méthodes ou attributs) : public, private et protected. Leur utilisation permet de définir des niveaux de protection différents (présentés dans un ordre croissant de niveau de protection offert):

| Modificateur | Rôle |
|--------------|------|
| public | Une variable, méthode ou classe déclarée public est visible par tous les autres objets. Depuis la version 1.0, une seule classe public est permise par fichier et son nom doit correspondre à celui du fichier. Dans la philosophie orientée objet aucune donnée d'une classe ne devrait être déclarée publique : il est préférable d'écrire des méthodes pour la consulter et la modifier |
| par défaut (vide) | Il n'existe pas de mot clé pour définir ce niveau, qui est le niveau par défaut lorsqu'aucun modificateur n'est précisé. Cette déclaration permet à une entité (classe, méthode ou variable) d'être visible par toutes les classes se trouvant dans le même package. |
| protected | Si une méthode ou une variable est déclarée protected, seules les méthodes présentes dans le même package que cette classe ou ses sous-classes pourront y accéder. On ne peut pas qualifier une classe avec protected. |
| private | C'est le niveau de protection le plus fort. Les composants ne sont visibles qu'à l'intérieur de la classe : ils ne peuvent être modifiés que par des méthodes définies dans la classe et prévues à cet effet. Les méthodes déclarées private ne peuvent pas être en même temps déclarées abstract car elles ne peuvent pas être redéfinies dans les classes filles. |

 [_source sur https://jmdoudoux.fr_](https://www.jmdoudoux.fr/java/dej/chap-poo.htm)