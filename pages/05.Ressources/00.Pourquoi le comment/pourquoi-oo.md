---
template: item
---

### Pourquoi la programmation orientée objet? C'est quoi déjà? 

Une vidéo qui explique pourquoi l'on a inventé la programmation objet; il s'agit de réduire la complexité d'un problème en le décomposant. Elle est excellente, simple, claire, mais en anglais:
<iframe width="560" height="315" src="https://www.youtube.com/embed/IcDKYxPH7zw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Quel sont les concepts importants de la programmation objet? A quoi cela sert-il?
<iframe width="560" height="315" src="https://www.youtube.com/embed/5j5z9BJCAW8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Et une introduction à la programmation objet de l'EPFL:
<iframe width="560" height="315" src="https://www.youtube.com/embed/n0_gC7YalUM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Pourquoi Java?

Nous avons choisi Java pour ce cours, puisqu'il s'agit d'un langage extrêmement répondu dans le métier. [Voir cette statistique...](https://www.tiobe.com/tiobe-index/)