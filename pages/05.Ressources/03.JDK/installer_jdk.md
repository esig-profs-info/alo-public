---
template: item
---

### Installation de Java pour le développement

Pour éxecuter une application Java déjà écrite, il suffit d'installer le JRE (Java Runtime Environement). C'est cela qui installé par défaut sur la plupart des ordinateurs.

En revanche, pour développer soi-même une application Java, il faut installer ce qui s'appelle le JDK (Java Developer Kit).


### Télécharger le JDK

[JDK 23, version OpenSource](https://jdk.java.net/23/)
