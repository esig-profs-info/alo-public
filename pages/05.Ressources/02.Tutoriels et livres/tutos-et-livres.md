---
template: item
---

### Java pour les enfants
Un livre en ligne expliquant clairement les concepts de base. La version anglaise explique également comment créer un projet en IntelliJ Idea, notre IDE de choix.

* [L’original en anglais](https://yfain.github.io/Java4Kids/)
* [Java pour les enfants - version française, versions de Java et outils passablement dépassés](https://java.developpez.com/livres-collaboratifs/javaenfants/)
* [Correction des exercices](https://laurent-bernabe.developpez.com/tutoriel/java/corrige-exercices-java-pour-enfants)


### Tutoriel Openclassrooms sur Java

[Page d'accueil du tutoriel](https://openclassrooms.com/fr/courses/26832-apprenez-a-programmer-en-java)