---
template: item
---

### Précis Java
Un résumé (_quick reference_) de la syntaxe Java, créé par M. Batard.

[Précis Java.pdf](PrecisJava.pdf)
