---
template: item
---

Pour lire des données d'une base de données SQLite en Java, vous pouvez utiliser le package JDBC (Java Database Connectivity). Voici les étapes clés pour se connecter et récupérer des données à partir d'une base SQLite :

## Inclure la bibliothèque SQLite JDBC

1. Télécharger le fichier JAR (Java Archive), par exemple [ici](https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.47.2.0/sqlite-jdbc-3.47.2.0.jar)
2. Créer un dossier `lib` à la racine de votre projet IntelliJ Idea, y ajouter le fichier .jar
3. Dans IntellIj Idea, clic droit sur le .jar qui se trouve maintenant dans votre dossier "lib", "Add as Library...". On peut laisser les options par défaut.

## Ajout de la base de données 

Copier le fichier de base de données fourni pour l'exercice dans la racine du projet. Par exemple `monexemple.sqlite`


## Exemple de code Java

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLiteExample {

    public static void main(String[] args) {
        // URL de connexion à la base de données SQLite
        String url = "jdbc:sqlite:monexemple.sqlite";

        // Connexion et manipulation de la base de données
        try (Connection conn = DriverManager.getConnection(url)) {
            
            // Si la connexion est établie avec succès
            if (conn != null) {
                System.out.println("Connexion à la base de données réussie !");

                // Lecture des données avec PreparedStatement
                String selectSQL = "SELECT * FROM utilisateurs WHERE age > ?";
                try (PreparedStatement stmt = conn.prepareStatement(selectSQL)) {
                    stmt.setInt(1, 25); // Exemple de paramètre pour filtrer par âge
                    
                    try (ResultSet rs = stmt.executeQuery()) {
                        // Affichage des résultats
                        while (rs.next()) {
                            int id = rs.getInt("id");
                            String nom = rs.getString("nom");
                            int age = rs.getInt("age");
                            System.out.println("ID: " + id + ", Nom: " + nom + ", Age: " + age);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

```