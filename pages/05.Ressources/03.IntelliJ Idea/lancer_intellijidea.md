---
template: item
---

### Installer IntelliJ Idea à la maison

[Télécharger ici](https://www.jetbrains.com/fr-fr/idea/download/#section=windows#noprocess). La Community Edition est largement suffisante pour nos besoins. Si vous souhaitez utiliser la version complète, vous devez souscrire une licence académique [ici](https://www.jetbrains.com/shop/eform/students).