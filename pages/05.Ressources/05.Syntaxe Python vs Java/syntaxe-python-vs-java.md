---
template: item
---

[Document comparatif (en anglais)](./AComparisonoftheSyntaxofPythonandJava-math-cs.gordon.edu.pdf)

Source: [http://math-cs.gordon.edu/courses/cps122/]

[comparaison de ```ArrayList``` (Java) et des listes en Python](./ComparaisonAL-P.pdf)

Source: [http://math-cs.gordon.edu/courses/cps122/]