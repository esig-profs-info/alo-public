---
content:
    items: '@self.children'
    order:
        by: folder
        dir: desc
---

## Espace du groupe 3 (ms)

### Valeurs
La _sécurité psychologique_, primordiale au travail d'équipe [Slides](https://docs.google.com/presentation/d/1x5KxXwlq0Q02tluLyR8F5eXhpFeu7FjDZWvv7ybEqjM/edit?usp=sharing)

[Normes collectives du groupe 3](https://docs.google.com/spreadsheets/d/1MP8uXbA3jwetGbiI5jf98elGpaxzCYj8a7IJmLK4aoo)

_Tree of Trust, la même chose autrement_ <a href="https://drive.google.com/open?id=1Z2RCcLqQIdeVlWoIk6YFnlt3dWruJH46">Illustration par Willy Wijnands, 2019</a>

### eduScrum Stories and plan
[Lien sur tableur](https://docs.google.com/spreadsheets/d/1tJRENRGpXYv1a02EkBLpu2hnvx19WilhrVFhFU6sCQ4/edit?usp=sharing)


### Vidéos du cours
[Vidéos filmés en cours](https://drive.google.com/drive/folders/1ixgi5JSQGqBZECKWjaixP1RHm14sgNYE?usp=sharing)

