---
template: item
published: true
---

===

# Exercices Jeu Personnages

**Objectifs**
* comprendre et appliquer les bonnes pratiques de visibilité
* comprendre l'héritage
* comprendre la redéfinition
* comprendre le polymorphisme dynamique

**NB**
* Ce TP est en construction, merci de votre indulgence
* Ce TP est à réaliser en classe avec l'aide d'un professeur

## Partie 1: Création des classes et des méthodes manquantes

### Contexte
Téléchargez le projet de jeu [ici](jeu_personnages.zip)

Pour vous aider, voici un petit diagramme des classes attendues:

![](diagramme_jeu.png)

Ajoutez les classes et les méthodes nécessaires pour que les codes suivants donnent les sorties attendue.

### Exemple de sortie 1
le code suivant dans la classe Main:

```java
public static void main(String[] args) {
        Villageois jpj = new Villageois("Jean Petit Jean", "bucheron");
        Guerrier brute = new Guerrier("brute épaisse", new Arme("épée",20));
        Magicien merlin = new Magicien("Merlin l'enchanteur");
        System.out.println("*******************");
        jpj.aborder(merlin);
        System.out.println("*******************");
        merlin.aborder(jpj);
        System.out.println("*******************");
        brute.aborder(jpj);
        System.out.println("*******************");
    }
```
doit donner la sortie suivante:

```
*******************
Jean Petit Jean> Bonjour, mon nom est Jean Petit Jean comment allez-vous?
*******************
Merlin l'enchanteur> Bonjour, mon nom est Merlin l'enchanteur comment allez-vous?
Merlin l'enchanteur> Tu m'a l'air un peu pâlot... je vais te soigner...
Merlin l'enchanteur> Je t'aurais bien soigné...mais je n'ai pas de baguette... désolé :(
*******************
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 80 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
*******************
```


### Exemple de sortie 2
le code suivant dans la classe Main:

```java
public static void main(String[] args) {
    Villageois jpj = new Villageois("Jean Petit Jean", "bucheron");
    Guerrier brute = new Guerrier("brute épaisse", new Arme("épée",20));
    Magicien merlin = new Magicien("Merlin l'enchanteur");

    brute.aborder(jpj);
    brute.aborder(jpj);
    brute.aborder(jpj);
    brute.aborder(jpj);
    brute.aborder(jpj);
}
```
doit donner la sortie suivante:

```
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 80 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 60 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 40 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 20 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> haaaaaaaaaaaaaa adieu monde cruel!
brute épaisse> RIP Jean Petit Jean tu n'aurais pas du croiser ma route
```

### Exemple de sortie 3
le code suivant dans la classe Main:

```java
public static void main(String[] args) {
    Villageois jpj = new Villageois("Jean Petit Jean", "bucheron");
    Guerrier brute = new Guerrier("brute épaisse", new Arme("épée",20));
    Magicien merlin = new Magicien("Merlin l'enchanteur");

    brute.aborder(jpj);
    merlin.aborder(jpj);
    brute.aborder(jpj);
    jpj.aborder(merlin);
}
```

doit donner la sortie suivante:
```
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 80 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
Merlin l'enchanteur> Bonjour, mon nom est Merlin l'enchanteur comment allez-vous?
Merlin l'enchanteur> Tu m'a l'air un peu pâlot... je vais te soigner...
Merlin l'enchanteur> Je t'aurais bien soigné...mais je n'ai pas de baguette... désolé :(
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Jean Petit Jean> ouch!... je n'ai plus que 60 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
Jean Petit Jean> Bonjour, mon nom est Jean Petit Jean comment allez-vous?
```

### Exemple de sortie 4
le code suivant dans la classe Main:

```java
public static void main(String[] args) {
        Villageois jpj = new Villageois("Jean Petit Jean", "bucheron");
        Guerrier brute = new Guerrier("brute épaisse", new Arme("épée",20));
        Magicien merlin = new Magicien("Merlin l'enchanteur");

        merlin.setBaguette(new Baguette("merlinator", 100));
        merlin.aborder(brute);
    }
```
doit donner la sortie suivante:
```
Merlin l'enchanteur> Bonjour, mon nom est Merlin l'enchanteur comment allez-vous?
Merlin l'enchanteur> Tu m'a l'air un peu pâlot... je vais te soigner...
brute épaisse> hiha je me sens mieux avec ces 100 points d'énergie supplémentaire, mon capital se monte à 200
Merlin l'enchanteur> et voilà, le tour est joué ho ho ho
```

## partie 2:
## 2A
### soit le main suivant (version noob, voir version pro plus loin):

```java
public static void main(String[] args) {
        Villageois jpj = new Villageois("Jean Petit Jean", "bucheron");
        Villageois eric = new Villageois("Eric Batard", "Enseignant ESIG");
        Villageois mirko = new Villageois("Mirko Steinle", "Enseignant ESIG");
        Villageois thomas = new Villageois("Thomas Servettaz", "Enseignant ESIG");
        Guerrier brute = new Guerrier("brute épaisse", new Arme("épée",20));
        Guerrier arnold = new Guerrier("Schwarzie", new Arme("massue",45));
        Guerrier rambo = new Guerrier("Rambo", new Arme("lance flamme",90));
        Guerrier alf = new Guerrier("Alf", new Arme("pistolet laser",120));
        Magicien elias = new Magicien("Elias De Kelliwic'h");
        Magicien merlin = new Magicien("Merlin l'enchanteur");
        merlin.setBaguette(new Baguette("merlinator I", 80));
        merlin.setBaguette(new Baguette("merlinator II", 90));
        elias.setBaguette(new Baguette("itribur", 60));

        //début de la promenade...
        rambo.aborder(mirko);
        elias.aborder(eric);
        alf.aborder(eric);
        brute.aborder(thomas);
        rambo.aborder(alf);
        alf.aborder(thomas);

        ArrayList<Personnage> equipe1 = new ArrayList<>();
        equipe1.add(jpj);
        equipe1.add(eric);
        equipe1.add(brute);
        equipe1.add(rambo);
        equipe1.add(merlin);

        ArrayList<Personnage> equipe2 = new ArrayList<>();
        equipe2.add(mirko);
        equipe2.add(thomas);
        equipe2.add(alf);
        equipe2.add(arnold);
        equipe2.add(elias);
        
    }
```

### Ajoutez le code nécessaire pour: 
1. permettre aux Personnages de retourner leur énergie (un getter?) sachant que:
* les magiciens et les guerriers sont des menteurs...
* les magiciens exagèrent leur énergie réelle de 20% lorsqu'on la leur demande...par exemple s'ils ont 100, ils diront 120
* les geurriers eux, exagèrent de 50%
* les villageois sont honnêtes

puis faites dire leur énergie à chaque membre de chaque équipe et ajoutez "--> GAME OVER" pour les Personnages tués:

####Sortie produite:
```
Rambo> Bonjour, mon nom est Rambo comment allez-vous?
Rambo> bim!
Mirko Steinle> ouch!... je n'ai plus que 10.0 points d'energie
Rambo> J'espère que t'as compris, que je te revoie plus ici!
Elias De Kelliwic'h> Bonjour, mon nom est Elias De Kelliwic'h comment allez-vous?
Elias De Kelliwic'h> Tu m'a l'air un peu pâlot... je vais te soigner...
Eric Batard> hiha je me sens mieux avec ces 60 points d'énergie supplémentaire, mon capital se monte à 160.0
Elias De Kelliwic'h> et voilà, le tour est joué ho ho ho
Alf> Bonjour, mon nom est Alf comment allez-vous?
Alf> bim!
Eric Batard> ouch!... je n'ai plus que 40.0 points d'energie
Alf> J'espère que t'as compris, que je te revoie plus ici!
brute épaisse> Bonjour, mon nom est brute épaisse comment allez-vous?
brute épaisse> bim!
Thomas Servettaz> ouch!... je n'ai plus que 80.0 points d'energie
brute épaisse> J'espère que t'as compris, que je te revoie plus ici!
Rambo> Bonjour, mon nom est Rambo comment allez-vous?
Rambo> bim!
Alf> ouch!... je n'ai plus que 10.0 points d'energie
Rambo> J'espère que t'as compris, que je te revoie plus ici!
Alf> Bonjour, mon nom est Alf comment allez-vous?
Alf> bim!
Thomas Servettaz> haaaaaaaaaaaaaa adieu monde cruel!
Alf> RIP Thomas Servettaz tu n'aurais pas du croiser ma route

~~~~~ ENERGIE EQUIPE 1 ~~~~~
moi, Jean Petit Jean ai 100.0 points d'énergie
moi, Eric Batard ai 40.0 points d'énergie
moi, brute épaisse ai 150.0 points d'énergie
moi, Rambo ai 150.0 points d'énergie
moi, Merlin l'enchanteur ai 120.0 points d'énergie

~~~~~ ENERGIE EQUIPE 2 ~~~~~
moi, Mirko Steinle ai 10.0 points d'énergie 
moi, Thomas Servettaz ai 0.0 points d'énergie  --> GAME OVER
moi, Alf ai 15.0 points d'énergie 
moi, Schwarzie ai 150.0 points d'énergie 
moi, Elias De Kelliwic'h ai 120.0 points d'énergie 
```

##2B (version pro):

A l'aide de la classe Scanner, lisez les fichiers contenus dans [le zip suivant](datas_csv_personnages.zip).
Basez-vous sur le fichier **datas_personnages.csv** qui contient la liste des équipes. Chaque équipe peut être construite en lisant le fichier csv qui lui appartient (afin de créer les personnages)
Notez qu'il y a des elfes dans les données qu'il s'agit d'ignorer.

1. Créer une classe Equipe afin d'encapsuler les données, une équipe possède un nom

```
sorties à venir...
```


2. calculer l'énergie (réelle ou déclarée, selon un paramètre) de chaque membre d'une équipe

```
sorties à venir...
```

3. calculer l'énergie (réelle ou déclarée, selon un paramètre) totale d'une équipe

```
sorties à venir...
```

4. calculer l'équipe qui est (réellement) la plus en forme

```
sorties à venir...
```

5. calculer l'équipe qui ment le plus

```
sorties à venir...
```

6. à suivre
