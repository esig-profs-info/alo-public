---
template: item
published: true
---

===

# Exercices TPG

## Partie 1: création de classes avec héritage

**Objectifs**
* créer une super classe
* créer des classes enfants héritant de la super classe avec constructeur et toString en utilisant _super_
* gérer des attributs privés à l'aide de getter et de setter

### Contexte
Les TPG (transports Publics Genevois) vous mandatent pour créer un logiciel de gestion de leurs différents moyens de transports.<br/><br/>
M. Micheli, chef de projet pour le développement de ce logiciel, vous donne les informations suivantes:<br/><br/>
> Pour le moment nous désirons que vous n'implémentiez que deux types de véhicules: les **_trams_** et les **_mouettes_** (les bateaux qui traversent la Rade). Tous deux sont des **_véhicules_** avec en commun:
> 1. un nombre de passagers maximum
> 1. un prix d'achat<br/><br/>
>
> Concernant les **_trams_**, il est nécessaire d'enregistrer aussi:
> 1. s'il est bidirectionnel ou pas (capable de rouler dans les 2 sens)
> 1. le nombre de wagons qu'il possède<br/><br/>
> NB: notez que seuls les trams de marque "Duewag-Vevey" ne sont pas bidirectionnels!
>
> Concernant les **_mouettes_**, il est nécessaire d'enregistrer _le nombre de gilets de sauvetage_.

### Création des classes
Créez les classes nécessaires pour représenter les trams et les mouettes.

Dans une classe de test, merci aussi de créer les objets suivants:
1. Une mouette de 50 places ayant coûté 80000 CHF
1. Une mouette de 60 places ayant coûté 85000 CHF
1. Un tram de 90 places de marque Bombardier, de 6 wagons ayant coûté 640000 CHF.
1. Un tram de 80 places de marque Duewag-Vevey, de 4 wagons, ayant coûté 430000 CHF.

Assignez ensuite 45 gilets de sauvetages à la première mouette et 62 à la deuxième.
<br/><br/>
_Affichez ensuite ces objets dans la console, vous devriez obtenir le résultat suivant:_<br/>
```
vehicule (prix: CHF 80000) de type:  Mouette avec 45 gilets de secours
vehicule (prix: CHF 85000) de type:  Mouette avec 62 gilets de secours 
vehicule (prix: CHF 640000) de type:  Tramway (bidirectionnel) composé de 6 Wagons
vehicule (prix: CHF 430000) de type:  Tramway (monodirectionnel) composé de 4 Wagons
```

### Problème des gilets de sauvetage
M. Micheli souligne qu'il est important de vérifier qu'il y ait **toujours assez de gilets de sauvetage pour tous les passagers ainsi que pour le conducteur**. Merci d'implémenter le nécessaire pour vérifier cela en tout temps. 
Ainsi, si le nombre de gilets est insuffisant, modifiez l'affichage de la Mouette en question en ajoutant "PAS ASSEZ DE GILETS!"

Ce qui devrait vous donner l'affichage suivant:
```
vehicule (prix: CHF 80000) de type:  Mouette avec 45 gilets de secours (PAS ASSEZ DE GILETS!)
vehicule (prix: CHF 85000) de type:  Mouette avec 62 gilets de secours 
vehicule (prix: CHF 640000) de type:  Tramway (bidirectionnel) composé de 6 Wagons
vehicule (prix: CHF 430000) de type:  Tramway (monodirectionnel) composé de 4 Wagons
```

### Problème des gilets de sauvetage: améliorons le code
M. Micheli revient vers vous en soulignant que l'affichage de la mention "PAS ASSEZ DE GILETS!" n'est pas suffisant. Il voudrait qu'il soit **impossible** de se retrouver avec un nombre de gilets de sauvetage insuffisant pour une mouette. Quelle solution pouvez-vous lui proposer?

## Partie 2: Gestion des lignes

### Contexte
On vous demande aussi de gérer les lignes de bus dans le logiciel. Pour commencer, récupérez le [projet suivant](projet_partie_2.zip) qui inclut :
* la classe **Bus**
* la classe **Trolley**
* la classe **Vehicule**
* la classe **Ligne**
* la classe de test **TestLignes** (qui contient le ```main```)
* un fichier de données **lignes.csv** contenant les différentes lignes disponibles
* un fichier de données **vehicules.csv** contenant les bus et trolleys qui officient sur les lignes

Les classes _Bus_ et _Trolley_ héritent de la classe _Vehicule_. A vous d'adapter le code pour lire le fichiers **vehicules.csv**. Attention, il n'y a pas les mêmes données à lire sur chaque ligne s'il s'agit d'un bus ou d'un trolley. Par ailleurs, il est spécifié pour chaque véhicule, sur quelle(s) ligne(s) il officie. Si un véhicule est utilisé sur plusieurs lignes, elles sont alors séparées par le caractère "-". Nous vous conseillons d'essayer la méthode ```split()```de la classe String [voir ici par exemple](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html#split-java.lang.String-). Ensuite, pour assigner les lignes aux véhicules, nous vous laissons décider :)

La classe _Ligne_ est caractérisée par:
* le nom de la ligne (une lettre ou un numéro habituellement)
* une liste contenant le nom des Arrêts (dans l'ordre du parcours)
* une liste contenant la durée en minutes que le véhicule utilisera pour se rendre à l'arrêt (depuis le premier arrêt, celui en position 0)

#### Conseils pour la lecture des lignes
Le fichier **lignes.csv** contient tous les arrêts (dans l'ordre) de 6 lignes (3 bus et 3 trolleys), il s'agit donc de créer une nouvelle _ligne TPG_ lorsque le nom de la ligne change... à vous de trouver une astuce. Par ailleurs, pour savoir si une ligne est équipée pour les trolleys, il est nécessaire de lire le fichier **lignes_trolleys.txt** qui liste les lignes de trolleys, chacune associée à sa tension électrique (sur la même ligne).

Une fois la ligne créée, il s'agit de remplir les listes d'arrêts et de minutage correctement. Pour ce point nous vous conseillons d'ajouter une méthode _ajouterArret()_ et une autre _ajouterMinutage()_ dans la classe _Ligne_.

_Voici une image des données ouvertes dans un tableur: (des lignes ont été masquées):_<br/>

![illustration datas lignes](illustration_datas_lignes.png?cropResize=800,1001)

### travail demandé
En organisant votre code de la façon la plus optimisée possible (création de méthodes non demandées):

1. Créez sur la base de la lecture des fichiers csv, une liste des vehicule existants ainsi qu'une liste des lignes
1. Affichez le nombre de bus et le nombre de trolleys
1. Affichez le montant total d'investissement (somme des prix d'achat) représenté par tous les véhicules
1. Affichez le nom de la ligne qui utilise le plus de véhicules (est-ce une ligne de bus ou de trolley ?) 
1. Créez puis affichez la liste des véhicules ayant dépassé les 300'000 km
1. Trouvez puis affichez le véhicule qui a le moins de kilomètres au compteur
1. Il faut savoir que les trolleys doivent partir en contrôle technique tous les 200'000 km et les bus tous les 50'000 km, qu'un contrôle de trolley coûte 8000 CHF et celui d'un bus 6500 CHF. Par ailleurs, tous les véhicules, lors du contrôle, passent par un rafraîchissement des sièges et de l'intérieur qui coûte 150 CHF / passager. De plus, les trolleys équipés d'un moteur de secours voient le coût du contrôle augmenter de 1200 CHF.<br>
Ainsi:
	* Affichez pour chaque véhicule ce qu'il a coûté en entretien.
	* Simulez ensuite le coût d'entretien si l'on ajoute un contrôle annuel (tous les ans à partir de la date d'aquisition) coûtant de manière fixe, 2000 CHF pour les trolleys et 1500 CHF pour les bus.
	* Affichez la liste des véhicules qui coûtent plus cher en entretien journalier que la moyenne de tous les véhicules.

1. Affichez la liste de toutes les lignes avec, pour chaque ligne, son nombre d'arrêts
1. Créez puis affichez la liste des lignes possédant plus de _n_ arrêts (_n_ paramètrable)
1. Affichez le nombre de kilomètres total du réseau, sachant que les bus roulent en moyenne à 40 km/h et les trolleys à 37 km/h.
1. Affichez si l'assignation des trolleys aux lignes est correcte (en vérifiant que la tension des lignes est compatible avec la tension nécessaire pour faire avancer le trolley; si ce n'est pas le cas, affichez un message en spécifiant les véhicules et les lignes concernés)
1. Affichez les arrêts qui appartiennent à plusieurs lignes (*tricky*) en indiquant les noms des arrêts et des lignes concernés.
1. Créez une méthode _dureeParcours()_ qui prend en paramètre deux arrêts. Cette méthode retourne la durée de parcours entre les 2 arrêts. Nous considérons aussi que les lignes sont parcourues dans les deux sens via le même chemin.
