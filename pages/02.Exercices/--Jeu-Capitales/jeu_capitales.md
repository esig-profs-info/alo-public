---
template: item
published: true
---

===

# Exercices Jeu-Capitales

![jeu-capitales-image](jeucapitales.png?cropResize=525,300)

## Contexte:

On vous demande de créer un jeu en ligne de commande de connaissance géographique.

1. Le jeu se base sur les données contenues dans le fichier [capitales.csv](capitales.csv)
2. Le jeu consiste à trouver la capitale à partir d'un pays
3. Le jeu propose ensuite 10 questions (les unes après les autres): Par exemple "Quelle est la capitale de la Belgique?"
4. A vous de compter les points et d'imaginer les textes affichés selon les résultats

### Dans une version 1:
=> le joueur doit taper (exactement) le texte de la réponse

### Dans une version 2: 
=> le jeu propose 3 réponses (dont la bonne) (tapez 1,2 ou 3)

### Dans une version 3: 
=> le jeu demande d'abord au joueur (ou la joueuse) s'il (elle) souhaite répondre en mode :
* _duo_ (le joueur choisi parmi 2 propositions et obtient 1 point s'il répond juste)
* _carré_ (le joueur choisi parmi 4 propositions et obtient 3 points s'il répond juste)
* _cash_ (le joueur tape la réponse et obtient 5 points s'il répond juste)

Ensuite la question est posée selon le mode choisi.

## Bonus à choix:
1. Proposez un mode inversé (à choix au début du jeu) --> trouver le pays selon la capitale.

2. Proposez un mode multi-joueurs.


## Information:
=> Utilisez la classe `HashMap`

### Cadeau:
```
//Pour sortir une clé d'un HashMap<String,String> au hasard:

String getRandomKeyInHashMap(HashMap<String,String> hm){
   ArrayList<String> keysList = new ArrayList<String>(hm.keySet());
   int noQuestionAuHasard = getRandomNumberInRange(0,keysList.size());
   return keysList.get(noQuestionAuHasard);
}

int getRandomNumberInRange(int min, int max) {
   if (min >= max) {
       throw new IllegalArgumentException("max must be greater than min");
   }
   Random r = new Random();
   return r.nextInt((max - min) + 1) + min;
}

```
