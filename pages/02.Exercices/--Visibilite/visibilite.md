---
template: item
published: true
---

===
 
# Exercices Visibilité

![visibilite](codeagame.png?cropResize=525,300)

## Contexte:
Dans l'entreprise "Esig Games ltd" on crée des jeux.<br/>
Cette entrerprise est composée des équipes suivantes:

```
Team "les testeurs"
******************
=> l'équipe chargée de tester le programme final comme les utilisateurs finaux
```
```
Team "les devs 1"
*****************
=> l'équipe de développeurs java qui a créé le programme "Super Jeu" dans la classe Main
=> dev-utilisateurs (externe) de la classe Account
=> dépendants de la team "dev 2"
```
```
Team "les devs 2"
****************
=> l'équipe de développeurs java qui a créé la classe Account
=> dev-programmeurs (interne) de la classe Account
=> aucun lien avec les devs 1 (les membres ne se préoccupent pas des devs 1)
```

#### Soit le programme de base suivant (écrit par les devs 1):

```
import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        String choix = JOptionPane.showInputDialog(null, "Bonjour, pour créer un utilisateur, tapez 'c' ", "Bienvenue sur notre plateforme");

        if("c".equals(choix)) {
            String nom = JOptionPane.showInputDialog(null, "Entrez votre nom", "Création du compte");
            String pass = JOptionPane.showInputDialog(null, "Entrez votre mot de passe", "Création du compte");
            String email = JOptionPane.showInputDialog(null, "Entrez votre email", "Création du compte");
            Account u = new Account(nom,pass);
            u.email = email;
            JOptionPane.showMessageDialog(null,"Votre utilisateur a été créé", "Bravo !");
            System.out.println(u);

            choix = JOptionPane.showInputDialog("Pour modifier le mot de passe de l'utilisateur, tapez 'm'", "Modification du compte");
            if("m".equals(choix)){
                String new_pass = JOptionPane.showInputDialog(null, "Entrez le nouveau mot de passe", "Modification du compte");
                u.password = new_pass;
                JOptionPane.showMessageDialog(null, "Votre utilisateur a été modifié", "Bravo !");
                System.out.println(u);
            }
            else{
                JOptionPane.showMessageDialog(null, "Vous n'avez pas tapé 'c'...", "Au revoir !");
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Vous n'avez pas tapé 'c'...", "Au revoir !");
        }
    }
}
```

#### et la classe Account (écrite par les devs 2): 

```
import java.time.LocalDate;

public class Account {
    private String login;
    public String password;
    public LocalDate dateCreation;
    public String email;

    public Account(String login, String password) {
        this.login = login;
        this.password = password;
        dateCreation = LocalDate.now();
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", dateCreation=" + dateCreation +
                ", email='" + email + '\'' +
                '}';
    }
}
```

#### Effectuez le travail suivant 
**(attention, vous allez vous mettre dans la peau de différents employés de la société...)**

1. vous êtes: un(e) membre des testeurs: exécuter le programme et essayer les différent cas possibles
2. vous êtes: un(e) membre des devs 1: votre manager vous demande de faire vérifier l'ancien mot de passe avant de le modifier
3. vous êtes: un(e) membre des devs 2: votre manager vous demande de faire vérifier l'existence d'un "@" et d'au moins un "." dans toute adresse email
4. vous êtes: un(e) membre des devs 2: votre manager vous demande de faire vérifier l'ancien mot de passe avant de le modifier
5. vous êtes: un(e) membre des devs 1: votre manager vous demande de créer une team "dev 0" responsable d'encapsuler le programme dans une classe " Superjeu ". Vous devenez donc dev interne de cette classe et devez décider de la visibilité de ses attributs et fonctions
