---
template: item
published: true
---

Exercices pour découvrir la syntaxe Java.

===

### Afficher des pyramides
##### info préliminaire:
Pour afficher une nouvelle ligne dans la console, en Java, on écrit:
```
System.out.println("un texte sur une nouvelle ligne");
```
Pour afficher du texte sur la ligne courante, en Java, on écrit:
```
System.out.print("du texte sur la ligne courante");
```

#### Pyramide no1
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
*****
****
***
**
*
```

#### Pyramide no2 
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
*
**
***
****
*****
```

#### Pyramide no3 
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
   *
  ***
 *****
*******
```
Et pour un nombre d'étoile variable?

#### Pyramide no4 
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
12345
2345
345
45
5
```

#### Pyramide no5 
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
54321
4321
321
21
1
```

#### Pyramide no6 
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
111111
22222
3333
444
55
6
```

#### Pyramide no7 
Produisez le code nécessaire à afficher dans la console la pyramide suivante:
```
COMPUTER
COMPUTE
COMPUT
COMPU
COMP
COM
CO
C
```

