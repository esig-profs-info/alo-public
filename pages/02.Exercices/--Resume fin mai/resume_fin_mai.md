---
template: item
published: true
---

===

# Ex Résumé à fin mai

## Objectifs:

1. Créer des classes héritant d'un classe mère, ayant des attributs / méthodes commun(e)s
2. Utiliser des attirbuts mutli-valués
3. Créer et utiliser une HashMap
4. Comprendre le transtypage
5. Revoir des algorithmes connus
6. Résoudre (et comprendre) un problème complexe pour lequel "appliquer l'algorithme" ne suffit pas

## Travail

Par équipe de 2 (ou 3):

1. Trouver un contexte d'héritage (4 classes au minimum, une super, au moins deux sous-classes).
2. Une des classes doit posséder un attribut multi-valué (une liste) contenant des objets d'une autre classe.
3. Une liste d'objets de la superclasse doit être créée (contenant des objets des sous-classes) (accepté depuis le main)
4. Une de vos classes doit contenir un HashMap
5. Effectuer un traitement simple sur la liste du point 3 (moyenne, somme, min, max, dénombrement) n'impliquant pas de transtypage
6. Effectuer un traitement simple sur la liste du point 3 (moyenne, somme, min, max, dénombrement) impliquant le transtypage
7. Effectuer un traitement complexe sur la liste du point 3 (sur un attribut multivalué, comparaison de listes, filtre, …)

Attention: Il est nécessaire que vous définissiez correctement la visibilité des attributs et des méthodes que vous allez créer (vous devez être capables de justifier vos choix)
