---
template: item
published: true
---

# Scanner sur String

### Objectifs: 
> - Savoir utiliser la classe java.util.Scanner pour parser des chaînes de caractères.

### Ex. 1 : Liste de mots

Ecrivez une méthode qui prend en paramètre une phrase sous forme de chaîne de caractères et qui retourne une liste (ArrayList)
contenant les mots de la phrase.
Par "mot" nous entendons ici n'importe quelle suite de lettres ou des signes qui ne sont pas séparés par un espace.
Un "mot" doit contenir au moins 2 caractères.


Testez (au moins) avec les phrases ci-dessous.

```
"Hello world !" -> [Hello, world]

"Ada Lovelace est principalement connue pour avoir réalisé le premier véritable programme informatique, lors de son travail sur un ancêtre de l'ordinateur : la machine analytique de Charles Babbage."
```

### Ex. 2 : Additionner des nombres

Ecrivez une méthode qui prend en paramètre une chaîne de caractères contenant des nombres séparés par des espaces et qui en retourne la somme.

Par exemple:
```
"1 2 3" -> 6

"3 4 3,5 -20" -> -9,5

"" -> 0 (chaîne vide donne 0).
```
### Ex. 3 : Additionner des nombres en ignorant le bruit

Ecrivez une méthode qui prend en paramètre une chaîne de caractères contenant des nombres séparés par des espaces et qui en retourne la somme.
Cette chaîne contient également des mots qui ne sont pas des nombres et qu'il faut ignorer.

Par exemple:
```
"1 dix 2 blabla 3 Schtroumpf" -> 6

"3 un deux 4 3,5 -20" -> -9,5

"deux" -> 0
```
### Ex. 4 : Additionner des quantités de fruits

Ecrivez une méthode qui prend en paramètre une chaîne de caractères contenant des noms de fruits suivis par une série d'entiers. 

Pour chaque fruit, additionner les entiers qui le suivent pour obtenir le total, et afficher le résultat.

Par exemple:

```"Bananes 10 3 Poires 5 Pommes 1 2 1"```
 
devrait afficher
```
Bananes: 13
Poires: 5
Pommes: 4
```

Votre programme doit pouvoir s'adapter à des contenus variables. Par exemple:

```"Bananes 1 3 2 5"```

doit afficher

```Bananes: 11```

