---
template: item
published: true
---

# Concerts

## Objectifs: 
> - comprendre l'utilité de public / private
> - réaliser le bien-fondé de l'organisation du code en classes / objets


## Informations

Les salles de concerts se trouvent dans des villes. Une ville possède les attributs suivants:
- nom
- nbMaxSpectateursAuMetreCarre

Un concert se tient dans une salle de concert. La salle de concert possède les attributs suivants:
- ville
- largeurSalle
- longueurSalle
- largeurScène
- longueurScène

Enfin, un concert possède les attributs suivants:
- nomArtiste
- date
- complet (oui ou non) 
- nbTicketsVendus
- salle

### Informations supplémentaires:<br>
Lorsque le nombre de tickets vendus change, il est nécessaire de vérifier que le nombre maximum de personne par mètre carré de la salle n'est pas atteint.<br>
Pour cela, un calcul dépendant de la ville est à faire.<br>
Si le maximum est atteint, il est nécessaire de bloquer la vente des billets (par exemple en utilisant l'attribut booléen complet) 

## Votre travail

- Créer les classes nécessaires et réfléchissez pour chaque attribut/méthode s'il doit être private ou public.
- Le concert a une méthode `acheterBillet` qui ajoute un billet vendu et retourne vrai, si l'achat est possible.
- Si votre solution ne contient pas déjà une classe `Billeterie`, ajoutez là. Cette classe doit contenir au minimum:
	1. un attribut de type "tableau de concert"
	2. un constructeur qui prend en paramètre un "tableau de concert" et qui permet d'initialiser l'attribut du même type.
	3. une méthode `simulerAchats` qui simule l'achat de `n` billets pour chacun des concerts qu'elle gère. <br>*Attention, les concerts seront peut-être complets avant l'achat des `n` billets, c'est-à-dire qu'il faut arrêter la boucle (qui fait `n` tours au maximum) si le concert est complet.*
- Dans la classe `Main`:
	1. Créer une billeterie contenant les concerts suivants:
		> - Concert de Mozart, le 25 juillet 2020 dans la salle Stravinsky de Montreux (75m de long / 20m de large, scène de 20m de large, 10m de long). (3 pers/m^2 à Montreux)
		> - Concert de Chopin, le 26 juillet 2020 dans la salle Stravinsky de Montreux (75m de long / 20m de large, scène de 20m de large, 10m de long). (3 pers/m^2 à Montreux)
		> - Concert de Stromae, le 10 octobre 2020 dans la salle Arena de Bruxelles (100m de long / 30m de large, scène de 30m de large, 15m de long). (2.7 pers/m^2 à Bruxelles)
		> - Concert d'Angèle, le 13 mars 2020 dans la salle Chat Noir de Carouge (15m de long / 6m de large, scène de 6m de large, 3m de long). (3.8 pers/m^2 à Carouge)
	2. Lancer la simulation pour l'achat de 8000 billets.
	3. Afficher la billeterie avec les concerts qu'elle contient (résultats attendus ci-dessous) à l'aide de `toString()`. Il faudra définir une méthode `toString()` dans les classes `Billeterie` et `Concert`.


## Résultats attendus:
>Artiste: Mozart 	 | 	 Date: 25.07.2020 	 | 	 billets vendus: 3900<br/>
>Artiste: Chopin 	 | 	 Date: 26.07.2020 	 | 	 billets vendus: 3900<br/>
>Artiste: Stromae 	 | 	 Date: 10.10.2020 	 | 	 billets vendus: 6885<br/>
>Artiste: Angèle 	 | 	 Date: 13.03.2020 	 | 	 billets vendus: 273<br/>