---
template: item
published: true
---

# **Exercice Planètes**

![planetes logo](media/logo.jpg?cropResize=600,300)

## **Objectifs**
L'objectif de cet exercice est de renforcer vos compétences en programmation orientée objet et en manipulation de bases de données en Java. Vous allez notamment travailler sur :  

1. **Concepts fondamentaux en Java :**
   - Différence entre **classe utilisée** et **classe à utiliser**,
   - Surcharge et rôle de la méthode `toString()`,
   - Fonctionnement et rôle du constructeur,
   - Utilisation des **getters et setters** pour le contrôle des accès aux données.  

2. **Manipulation de collections dynamiques :**
   - Utilisation de la classe `ArrayList` pour stocker des objets.  

3. **Utilisation d'une base de données SQLite :**
   - Lire et récupérer des données à partir d'une base SQLite.

4. **Travail avec les données astronomiques :**
   - Manipuler et analyser des données relatives aux planètes et à leurs satellites.

5. **Implémentation d'algorithmes courants :**
   - Recherche et extraction d’informations utiles,
   - Calculs statistiques basés sur les caractéristiques des planètes.  

**Mais aussi :**  

6. **Concevoir une solution à partir d’un besoin métier**,  
7. **Prendre du plaisir à coder !** 🚀 

## **Mise en situation**
Le système solaire est composé de plusieurs planètes, certaines possédant des satellites en orbite autour d'elles. Vous devez développer un programme permettant de récupérer et analyser les informations des planètes et de leurs satellites à partir d'une base de données SQLite.

## **Matériel à disposition**
Une base de données SQLite [planetes](media/planetes.zip) contenant les informations des planètes et de leurs satellites.

## **Caractéristiques du modèle de données**
1. Une planète possède:
- **Nom** de la planète,  
- **Rayon moyen** (en km),  
- **Circonférence d’orbite** (en km),  
- **Liste de satellites** (qui peut être vide). 
2. Une classe `Planete` doit être créée pour modéliser les planètes et leurs caractéristiques.
3. Une classe `SystemeSolaire` doit être implémentée pour gérer la récupération des données et proposer différentes analyses.

**Attention: les nombres représentant la circonférence de l'orbite sont "astronomiques", c'est dire très élevés. Ils dépassent la valeur maximale que l'on peut stocker en Java avec le type `int`. Utilisez donc le type `long` (un entier long) partout où l'orbite est concerné (et `resultSet.getLong` pour la lecture).**

## **Organisation du programme**
Le programme doit être organisé avec une classe principale `Main` qui :
1. Instancie un objet de type `SystemeSolaire`,
2. Appelle les méthodes de la classe `SystemeSolaire` pour charger les données depuis la base SQLite ainsi qu'afficher les planètes et les statistiques. 

## **Fonctionnalités du programme**

### **1. Récupération des données**
Le constructeur de la classe `SystemeSolaire` se connecte à la base de données SQLite et charge les données dans une `ArrayList<Planete>`.

### **2. Affichage des planètes**
Une méthode `afficherPlanetes()` dans la classe `SystemeSolaire` affichera la liste des planètes du système solaire avec leurs caractéristiques.

```console
===== Liste des planètes =====
  - Mercure [rayon: 2439 km] [circonference de l'orbite: 359976856 km] [aucun satellite]
  - Venus [rayon: 6051 km] [circonference de l'orbite: 679892378 km] [aucun satellite]
  - Terre [rayon: 6371 km] [circonference de l'orbite: 939887974 km] [1 satellite]
  - Mars [rayon: 3389 km] [circonference de l'orbite: 1429085052 km] [2 satellites]
  - Jupiter [rayon: 69911 km] [circonference de l'orbite: 4887595931 km] [4 satellites]
  - Saturne [rayon: 58232 km] [circonference de l'orbite: 8957504604 km] [7 satellites]
  - Uranus [rayon: 25362 km] [circonference de l'orbite: 18026802831 km] [5 satellites]
```

### **3. Affichage des statistiques**
1. Une méthode `afficherStatistiquesPlanetes()` dans la classe `SystemeSolaire` affichera les statistiques suivantes:
    - La planète avec la plus grande circonférence d'orbite.
    - La planète avec la plus petite circonférence d'orbite.
    - Le nombre total de satellites du système solaire.
    - La moyenne des rayons des planètes.
```console
===== Statistiques =====
Uranus a la plus grande circonférence de l'orbite Mercure a la plus petite circonférence de l'orbite.
Le nombre de satellites total de ces planètes est de 19.
Le rayon moyen de ces planètes est de 24536.43 km.
```

2. Une méthode `afficherPlanetesSansSatellite()` dans la classe `SystemeSolaire` indique s'il existe au moins une planète sans satellite, en précisant son nom si c'est le cas.
```console
Dans cette liste, il y a au moins une planète qui ne possède aucun satellite, par exemple Mercure.
```
Ou, si toutes les planètes ont au moins un satellite :
```console
Toutes les planètes ont au moins un satellite.
```

3. Une méthode `afficherPlanetesAvecPlusieursSatellites()` dans la classe `SystemeSolaire` affichera la liste des planètes possédant au moins deux satellites, en précisant leurs noms si elles existent. Si aucune planète ne remplit cette condition, un message indiquera l'absence de telles planètes.
```console
===== Planètes avec plusieurs satellites =====
  - Mars dont les satellites sont : Phobos Deimos
  - Jupiter dont les satellites sont : Io Europa Ganymede Callisto
  - Saturne dont les satellites sont : Mimas Enceladus Tethys Dione Rhea Titan Lapetus
  - Uranus dont les satellites sont : Miranda Ariel Umbriel Titania Oberon
```
Ou, si aucune planète n'a plus d'un satellite :
```console
Aucune planète n'a 2 satellites ou plus.
```

---

**Bon courage et bon codage !** 🚀

