---
template: item
published: true
---
<head>
  <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
  <script id="MathJax-script" async
          src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
  </script>
</head>
===
 
# Exercices sur les tableaux 2D

Nous allons utiliser un tableau à deux dimensions pour représenter une grille de Sudoku.

<small><small>Notez que ces grilles ont le même nombre de lignes et de colonnes (9 en l'occurrence), ce qui n'est pas une obligation pour les tableaux 2D.</small></small>

Voici une grille de Sudoku ([source](https://upload.wikimedia.org/wikipedia/commons/f/ff/Sudoku-by-L2G-20050714.svg))

![](grille1.png)

Pour travailler sur ce genre de grille, vous allez définir une classe Java appelée ```GrilleSudoku``` dans un fichier ```GrilleSudoku.java```.

## Création de la classe ```GrilleSudoku```

Dans cette classe, vous définirez une constante ```TAILLE_GRILLE``` ainsi
```
public static final int TAILLE_GRILLE = 9;
```
et un tableau :
```
private int[][] grille = new int[TAILLE_GRILLE][TAILLE_GRILLE];
```

## Remplissage d'une grille 

Vous ajouterez à la classe ```GrilleSudoku``` un constructeur qui prendra en paramètre ***un nom de fichier***.

C'est en lisant les 81 (= 9*9) nombres dans un fichier texte que vous pourrez remplir une grille.
Par convention une case vide d'une grille sera représentée par la valeur 0.

A l'évidence il faudra aussi prévoir une méthode d'affichage pour vérifier que la lecture s'est bien déroulée.

Voici deux fichiers pour vos tests, à faire dans une classe de tests à part ```TestSudoku``` :
[Grille1.txt](Grille1.txt), 
[Grille2.txt](Grille2.txt) et 
[Grille3.txt](Grille3.txt)

#### Indications
Les données étant séparées par des espaces, vous pouvez utiliser un  ```Scanner``` sur fichier standard, en lisant nombre par nombre, ou lire ligne par ligne avec la paire ```Scanner``` sur fichier et ```Scanner``` sur chaîne.

De plus, comme le nombre de données est fixe et connu à l'avance (81), vous pouvez ici utiliser la boucle ```for``` au lieu de ```while```.

## Utilisation de la grille 

1. Ecrire une méthode renvoyant un entier qui compte le nombre de cases dont la valeur n'est pas comprise entre 0 et 9. Testez-la sur les grilles fournies dans la classe de test (Grille1 -> O, Grille2 -> 3)

2. Ecrire une méthode booléenne qui indique si une grille est composée de données correcte ou pas. Dans un premier temps, vous pouvez utiliser la méthode de la question précédente ; mais vous êtes encouragés à trouver un autre algorithme qui s'arrête dès qu'on rencontre une erreur.
A tester dans la classe de test (Grille1 -> ```true```, Grille2 -> ```false```)

3. Ecrire une méthode booléenne qui indique si une grille est *remplie*, c'est-à-dire si toutes les cases ont une valeur entre 1 et 9 (ou si aucune n'a une valeur de 0). Ici encore vous êtes encouragés à trouver un moyen d'arrêter dès qu'on rencontre une valeur à 0.

4. Ecrire une méthode renvoyant un entier qui fait la somme d'une ligne dont le numéro est passé en paramètre. Dans la classe de test, appelez cette méthode pour chaque ligne de la grille et affichez les résultats.

5. Même exercice que pour la question précédente mais avec un numéro de colonne passé en paramètre.

6. Ecrire une méthode qui indique (<small><small>un peu rapidement</small></small>) si une grille de Sudoku est complète. Comme vous le savez sans doute, le principe est que chaque ligne et chaque colonne d'une grille complète contient les chiffres de 1 à 9. Cela signifie que la somme de chaque ligne et de chaque colonne doit être égale à 45 ( = \\( \sum_{i=1}^{9} i \\), c'est-à-dire la somme des nombres de 1 à 9).

<small><small>Pour ceux qui connaissent les règles du Sudoku : ici on ne se préoccupera pas de la somme sur les 9 zones carrées de 3 sur 3. Vous pouvez vous poser la question de comment y arriver mais ce sera pour le plaisir du casse-tête car c'est totalement hors champ.</small></small>
