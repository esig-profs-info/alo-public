---
template: blog
content:
    items: '@self.children'
    order:
        by: folder
        dir: asc
---

# Exercices avec Greenfoot

Cette section regroupe les exercices fait avec l'environnements de programmation pédagogique [Greenfoot](https://www.greenfoot.org).

Nous avons choisi cet outil parce qu'il illustre très bien les concepts objets importants tout en étant facile à prendre en main.

L'environnement est gratuit et peut être téléchargé [ici](https://www.greenfoot.org/download).