---
template: item
published: true
---

## Les hérissons gourmands

Découverte de l'environnement Greenfoot et de Java avec le scénario des hérissons gourmands.

### Partie 1
Lancer le scénario Greenfoot et répondre au [questionnaire suivant](https://docs.google.com/forms/d/e/1FAIpQLSfMFmO_Wecl6IOldB2EATa3goYbBzEU646K3p6FRniafjhfPA/viewform) en vérifiant par le scénario les différentes affirmations.

[Scénario](ScenarioHerissonsVierge.zip) Un _scénario_ correspond à un projet Greenfoot. Il faut télécharger l'archive, la dézipper et puis l'ouvrir depuis l'application Greenfoot.

### Partie 2
Apprendre la syntaxe Java en modifiant le scénario de base selon l'énoncé ci-dessous.

Produire un document (Google Sheet) qui met en parallèle la syntaxe Python avec celle de Java.

[Énoncé](IntroHerissonsGourmands.docx)

