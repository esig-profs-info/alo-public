import greenfoot.Greenfoot;
import greenfoot.World;

import java.util.List;

/**
 * Un jardin ou vivent les herissons.
 * 
 * @author Michael Koelling
 * @version 1.0
 */
public class Jardin extends World
{
    /**
     * Creation d'un nouveau "monde" avec 8x8 cellules de 60x60 pixels chacune
     */
    public Jardin() 
    {
        super(8, 8, 60);
        setPaintOrder(Herisson.class, Pomme.class);

        prepare();
    }

    public int getMaxPommesMangees()
    {
        int[] tableauPommesMangees = getTableauPommesMangees();
        int max = Integer.MIN_VALUE;
        for(int i = 0;i<tableauPommesMangees.length;i++)
        {
            if(tableauPommesMangees[i] > max)
            {
                max = tableauPommesMangees[i];
            }
        }
        return max;
    }
    
    public void afficherPommesMangees()
    {
        // cette ligne initialise un tableau avec le nombre de pommes mangees
        // pour chaque herisson du Jardin
        int[] tableauPommesMangees = getTableauPommesMangees();
        System.out.println("=== Résumé des pommes mangées ===");
        for(int i = 0;i<tableauPommesMangees.length;i++)
        {
            System.out.println("Pommes mangées ["+i+"] : " + tableauPommesMangees[i]);
        }
    }
    
    public void afficherPommesMangeesAvecNoms()
    {
        // cette ligne initialise un tableau avec le nombre de pommes mangees
        // pour chaque herisson du Jardin
        Herisson[] tableauHerissons = getTableauHerissons();
        String motPomme;
        System.out.println("=== Résumé des pommes mangées ===");
        for(int i = 0;i<tableauHerissons.length;i++)
        {
            if(tableauHerissons[i].nombreDePommesMangees > 1)
            {
                motPomme = " pommes";
            }
            else
            {
                motPomme = " pomme";
            }
            System.out.println(tableauHerissons[i].nom + " a mangé: " + tableauHerissons[i].nombreDePommesMangees + motPomme);            
        }
    }
    
    public int getHerissonGourmand(int limite)
    {
        int nbHerissons = 0;
        Herisson[] tableauHerissons = getTableauHerissons();
        for(int i = 0;i<tableauHerissons.length;i++)
        {
            if(tableauHerissons[i].nombreDePommesMangees >= limite)
            {
                nbHerissons++;
            }
        }
        return nbHerissons;
    }
    
    public void afficherGagnantPerdant()
    {
        Herisson[] tableauHerissons = getTableauHerissons();
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int posMax=0;
        int posMin=0;
        for(int i = 0;i<tableauHerissons.length;i++)
        {
            if(tableauHerissons[i].nombreDePommesMangees > max)
            {
                max = tableauHerissons[i].nombreDePommesMangees;
                posMax = i;
            }
            
            if(tableauHerissons[i].nombreDePommesMangees < min)
            {
                min = tableauHerissons[i].nombreDePommesMangees;
                posMin = i;
            }           
        }
        System.out.println(tableauHerissons[posMax].nom + " a gagné ("+max+" pomme(s))");
        System.out.println(tableauHerissons[posMin].nom + " a perdu ("+min+" pomme(s))");        
    }

    public void afficherGagnantsPerdants()
    {
        Herisson[] tableauHerissons = getTableauHerissons();
        int max = trouveLeMax(tableauHerissons);
        int min = trouveLeMin(tableauHerissons);

        for(int i=0;i<tableauHerissons.length;i++)
        {
            if(tableauHerissons[i].nombreDePommesMangees == max)
            {
                System.out.println(tableauHerissons[i].nom + " a gagné " + getMotPommesPluriel(max));
            }
            else if(tableauHerissons[i].nombreDePommesMangees == min)
            {
                System.out.println(tableauHerissons[i].nom + " a perdu " + getMotPommesPluriel(min));
            }
        }

    }
    
    private String getMotPommesPluriel(int i)
    {
        if(i>1)
        {
            return "("+i+" pommes)";
        }
        else
        {
           return "("+i+" pomme)";
        }
    }

    public int trouveLeMax(Herisson[] tab)
    {
        int max = Integer.MIN_VALUE;
        for(int i = 0;i<tab.length;i++)
        {
            if(tab[i].nombreDePommesMangees > max)
                max = tab[i].nombreDePommesMangees;
        }
        return max;
    }

    public int trouveLeMin(Herisson[] tab)
    {
        int min = Integer.MAX_VALUE;
        for(int i = 0;i<tab.length;i++)
        {
            if(tab[i].nombreDePommesMangees < min)
                min = tab[i].nombreDePommesMangees;
        }
        return min;
    }
  
    /**
     * Retourne le tableau du nombre de pommes mangees par herisson.
     */
    public int[] getTableauPommesMangees()
    {
        Herisson[] tableauHerissons = getTableauHerissons();
        int[] tableauPommesMangees = new int[tableauHerissons.length];
        for (int h = 0; h < tableauHerissons.length; h++)
        {
            tableauPommesMangees[h] = tableauHerissons[h].nombreDePommesMangees;
        }
        return tableauPommesMangees;
    }

    /**
     * Dispose un certain nombre de pommes de facon aleatoire dans le jardin.
     * Le nombre de pommes peut etre specifie.
     */
    public void randomApples(int howMany)
    {
        for(int i=0; i<howMany; i++) {
            Pomme pomme = new Pomme();
            int x = Greenfoot.getRandomNumber(getWidth());
            int y = Greenfoot.getRandomNumber(getHeight());
            addObject(pomme, x, y);
        }
    }

    /**
     * Retourne tous les herissons sous forme de tableau
     */
    private Herisson[] getTableauHerissons()
    {
        List<Herisson> listeHerissons = getObjects(Herisson.class);
        return listeHerissons.toArray(new Herisson[]{});
    }

    /**
     * Prepare le "monde" au demarrage du programme. C'est a dire: Creer les objets initiaux
     * et les ajouter au "monde".
     */
    private void prepare()
    {
        randomApples(10);
        Herisson herisson = new Herisson("Friedolin");
        addObject(herisson, 1, 2);        
        Herisson herisson2 = new Herisson("Elisabeth");
        addObject(herisson2, 3, 4);
        Herisson herisson3 = new Herisson("BigBoy");
        addObject(herisson3, 5, 6);
        Herisson herisson4 = new Herisson("FlashGordon");
        addObject(herisson4, 7, 8);
        Herisson herisson5 = new Herisson("Harrisson");
        addObject(herisson5, 6, 7);
        Herisson herisson6 = new Herisson("Elmo");
        addObject(herisson6, 5, 5);
    }
}
