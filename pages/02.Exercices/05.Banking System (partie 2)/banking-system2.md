---
template: item
published: true
---

# BankingSystem - partie 2

## 5 Des clients et des comptes

Puisque les comptes appartiennent à des clients, nous allons implémenter cette relation.

Un client peut avoir un ou plusieurs comptes (ou aucun tout au début).

Pour représenter la liste des comptes qu'un client détient nous allons utiliser la classe `ArrayList` qui représente une liste d'objets à laquelle on peut ajouter des objets sans devoir savoir au départ combien d'objet elle va devoir contenir (contrairement au tableau qui a une taille fixe).


#### 5.1 Ajouter un attribut, une méthode et un accesseur et compléter le constructeur

Dans la classe `Client` ajoutez un attribut nommé `accounts`.

Ajoutez un accesseur pour cet attribut.

Un accesseur est une méthode qui retourne la valeur d'un attribut. On le nomme get suivi du nom de l'attribut avec la 1ère lettre en majuscule (voir exercice 4.1.1). Donc ici ce sera `getAccounts`

Complétez le constructeur pour initialiser accounts.

Ajoutez une méthode nommée `addAccount` qui ajoute une instance de `Account` passé en paramètre à la liste des comptes du client.

#### 5.2 Tester

Ajoutez une méthode `testerCompteClient` a la classe `ClientTest`. Cette méthode crée un client, puis ajoute 2 comptes à ce client et affiche le résultat.

```
private static void testerCompteClient()
{
    Client client = new Client(101, "Daniel", "Smith", LocalDate.of(1980, 6, 5));
    Account acc1 = new Account("A1");
    client.addAccount(acc1);
    Account acc2 = new Account("A2");
    client.addAccount(acc2);
    System.out.println("Comptes: "  + client.getAccounts());
}
```

>Améliorez l'affichage de façon à afficher un compte par ligne.

## 6 Client, comptes

#### 6.1 Préparation des données
Nous allons maintenant créer plusieurs comptes. Tout d'abord, créer une méthode qui retourne une liste d'instances de `Account` dans la classe `ClientTest`.

Accounts:<br/>
_A111_<br/>
_A112_<br/>
_A121_<br/>
_A134_<br/>

#### 6.2 Liaison du client à ses comptes

Ajoutez une méthode `creerClientAvecComptes` à la classe `ClientTest`. Cette méthode va retourner un client initialisé avec les comptes spécifiés par la liste des Account ci-dessus.


#### 6.3 Tester

Ajouter une méthode `testerCreerComptes` dans la classe `ClientTest` et l'appeler depuis `main`:

```
private static void testerCreerComptes()
{
    Client client = creerClientAvecComptes();
    System.out.println("Client: " + client);
    System.out.println(client.getAccounts());
}
```
> Comme en 5.2, améliorez l'affichage de façon à afficher un compte par ligne.

## 7 Traitement de liste d'opérations sur les comptes

#### 7.1 Préparation des données

Nous allons maintenant devoir effectuer une série d'opérations sur les comptes du client spécifiée par la liste des opérations que vous pouvez obtenir à l'aide de la méthode ci-dessous qui est à ajouter dans la classe `ClientTest`. A vous de créer la classe `Operation`.

```
    private static ArrayList<Operation> getOperationsList() {
        ArrayList<Operation> lstOperation = new ArrayList<>();
        lstOperation.add(new Operation("A111", "credit", 50));
        lstOperation.add(new Operation("A112", "credit", 70));
        lstOperation.add(new Operation("A121", "credit", 100));
        lstOperation.add(new Operation("A111", "debit", 20));
        lstOperation.add(new Operation("A134", "credit", 200.5));
        lstOperation.add(new Operation("A121", "debit", 70.5));
        return lstOperation;
    }
```

#### 7.2 Traitement des opérations

Dans la classe Client, ajouter une méthode `executeOperations`. Cette méthode a comme paramètre une liste d'opérations. Est-ce qu'il est aussi nécessaire de passer l'instance de `Client` en paramètre, qu'en pensez-vous? Pour chaque ligne de la liste d'opérations, la méthode doit trouver le compte correspondant et effectuer l'opération (crédit ou débit).

Pour obtenir le compte avec un numéro de compte donné, ajouter à la classe Client une méthode `getAccount` avec comme paramètre un numéro de compte. Cette méthode doit retourner un Account si il existe. Elle retournera null si il n'y a pas de compte avec le numéro spécifié.

> Tips pour le développement: Pour tester l'égalité entre deux `String`, il est nécessaire d'utiliser la méthode `equals()` de la classe `String` (plutôt que le `==` habituel).

#### 7.2 Tester

Ajouter une méthode `testerOperations` dans la classe ClientTest et l'appeler depuis main:

```
private static void testerOperations()
{
    Client client = creerClientAvecComptes();
    ArrayList<Operation> operations = getOperationsList();
    client.executeOperations(operations); // Est-ce cette ligne?
    client.executeOperations(client, operations); // Ou bien cela? Choisissez!
    System.out.println(client);
    System.out.println(client.getAccounts());
}
```
> Comme en 5.2, améliorez l'affichage de façon à afficher un compte par ligne.

Si tout est correct, vous devez obtenir l'affichage suivant:

```
Client<id=101, nom='Smith, prénom='Daniel', date de naissance=05/06/1980>
Account<accountNumber='A111', balance=30.0>
Account<accountNumber='A112', balance=70.0>
Account<accountNumber='A121', balance=29.5>
Account<accountNumber='A134', balance=200.5>
```