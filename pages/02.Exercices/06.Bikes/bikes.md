---
template: item
published: true
---

# **Exercice Bikes**

![bikes logo](media/logo.png?cropResize=300,300)

## **Objectifs**
L'objectif de cet exercice est de renforcer vos compétences en programmation orientée objet et en manipulation de bases de données en Java. Vous allez notamment travailler sur :  

1. **Concepts fondamentaux en Java :**
   - Différence entre **classe utilisée** et **classe à utiliser**,
   - Surcharge et rôle de la méthode `toString()`,
   - Fonctionnement et rôle du constructeur,
   - Utilisation des **getters et setters** pour le contrôle des accès aux données.  

2. **Manipulation de collections dynamiques :**
   - Utilisation de la classe `ArrayList` pour stocker des objets.  

3. **Utilisation d'une base de données SQLite :**
   - Lire et récupérer des données à partir d'une base SQLite.

4. **Travail avec les dates :**
   - Manipuler et formater des dates avec la classe `LocalDate`.  

5. **Implémentation d'algorithmes courants :**
   - Calcul de la moyenne,
   - Recherche d’informations dans une liste d’objets.  

**Mais aussi :**  

6. **Concevoir une solution à partir d’un besoin métier**,  
7. **Prendre du plaisir à coder !** 🎉  

## **Mise en situation**
La société **MyBikes Sàrl** propose un service de réparation de vélos. Vous êtes chargé(e) de développer les premières fonctionnalités d’un programme en Java permettant d’informatiser la gestion des réparations et d’extraire des **statistiques utiles**.  

Vous disposerez d’une **base de données SQLite** contenant les réparations réalisées, que vous devrez exploiter dans votre programme.  

## **Matériel à disposition**
Une base de données SQLite [reparations](media/reparations.zip), contenant les informations des réparations effectuées.  

## **Caractéristiques du modèle de données**
1. Chaque **réparation** possède les informations suivantes :  
- **Date** de la réparation,  
- **Durée** (en minutes),  
- **Description** (obligatoire),  
- **Statut**.  
2. Une classe `Reparation` doit être créée pour modéliser les réparations et leurs caractéristiques.
3. Une classe `GestionReparations` doit être implémentée pour proposer différentes analyses.

### **Règles de gestion :** 
Le **prix d’une réparation** est calculé dynamiquement en fonction de la durée et d’un **tarif horaire fixé à CHF 120.-/heure**. Il n’est donc **pas stocké dans la base de données**.  

## **Organisation du programme**
Le programme doit être organisé avec une classe principale `Main` qui :
1. Lit les données de la base SQLite,
2. Instancie un objet `GestionReparations` en lui passant une liste de réparations,
3. Appelle les méthodes de la classe `GestionReparations` pour afficher les réparations et les statistiques.  

## **Fonctionnalités du programme**

### **1. Lecture des réparations**
Une méthode `chargerReparations()` de la classe `Main` établit une connexion à la base de données SQLite et récupère les données sous forme d'une `ArrayList<Reparation>`.

### **2. Affichage des réparations**
Une méthode `afficherReparations()` de la classe `GestionReparations` doit afficher dans la console toutes les réparations sous le format suivant :  

```console
===== Liste des réparations =====
Réparation n°1 Réparation(ouverte) du : 26/09/2016 --> changement_pneu_avant (38 min.)
Réparation n°2 Réparation(ouverte) du : 25/09/2016 --> changement_grand_plateau (117 min.)
Réparation n°3 Réparation(payée) du : 02/11/2016 --> changement_grand_plateau (112 min.)
Réparation n°4 Réparation(payée) du : 23/06/2016 --> graissage_chaîne (239 min.)
Réparation n°5 Réparation(ouverte) du : 30/05/2016 --> changement_grand_plateau (157 min.)
Réparation n°6 Réparation(payée) du : 15/01/2017 --> Redressage_des_rayons (127 min.)
Réparation n°7 Réparation(payée) du : 10/10/2016 --> changement_pneu_avant (137 min.)
Réparation n°8 Réparation(payée) du : 20/02/2017 --> changement_pneu_avant (78 min.)
Réparation n°9 Réparation(ouverte) du : 02/02/2017 --> graissage_chaîne (27 min.)
Réparation n°10 Réparation(ouverte) du : 05/03/2016 --> Redressage_des_rayons (102 min.)
```

### **3. Calcul et affichage du montant des réparations**
Une méthode `afficherMontants()` de la classe `GestionReparations` doit afficher le coût de chaque réparation en appliquant le tarif horaire.  

```console
===== Montants des réparations =====
Montant de la réparation n°1 : CHF. 76.0
Montant de la réparation n°2 : CHF. 234.0
Montant de la réparation n°3 : CHF. 224.0
Montant de la réparation n°4 : CHF. 478.0
Montant de la réparation n°5 : CHF. 314.0
Montant de la réparation n°6 : CHF. 254.0
Montant de la réparation n°7 : CHF. 274.0
Montant de la réparation n°8 : CHF. 156.0
Montant de la réparation n°9 : CHF. 54.0
Montant de la réparation n°10 : CHF. 204.0
```

### **4. Calcul des moyennes de durée**
Une méthode `afficherMoyennes()` de la classe `GestionReparations` doit calculer la **durée moyenne** des réparations en fonction de leur statut.

*Votre solution doit être fonctionnelle pour un nombre variable de statuts, et ne doit pas être limitée aux seuls statuts "ouverte" et "payée".*

```console
===== Moyennes de durée des réparations =====
Moyenne des durées des 5 réparations ouverte : 88.2 min.
Moyenne des durées des 5 réparations payée : 138.6 min.
```

### Tips pour le développement
Pour convertir une date contenue dans un `String` en `LocalDate`, vous pouvez utiliser la méthode `parse()` de la classe `LocalDate` selon l'exemple suivant: `LocalDate date = LocalDate.parse("2017-03-02");`

---

**Bon courage et bon codage !** 🚀

<!-- 
## Pour continuer (et corser un peu l'affaire)

1.  Faîtes en sorte que des durées négatives ne puissent pas être saisies par un utilisateur de la classe Reparation.

2.  Modifiez la classe Reparation pour faire en sorte que toutes les durées soient arrondies au quart d'heure supérieur. En effet, le patron de l'entreprise vous a informé que la facturation ne s'effectuait pas à la minute près.

3.  Créez une méthode statique afficherReparationApres( ) qui prendra en paramètre une date, affichera la liste des réparations postérieures à cette date.

    Tip: pour comparer deux dates vous pouvez utiliser les méthodes `isBefore` ou `isAfter`:
    java
    boolean notBefore = LocalDate.parse("2016-06-12")
        .isBefore(LocalDate.parse("2016-06-11"));
    boolean isAfter = LocalDate.parse("2016-06-12")
        .isAfter(LocalDate.parse("2016-06-11"));

4.  Créez une méthode statique reparationExisteOnDate( ) qui prendra en paramètre une date et qui retournera vrai si une réparation existe à cette date (algorithme de recherche).
   
    Tip: pour tester l'égalité de deux dates vous devez utiliser la méthode `equals` et non `==` (comme pour les `String`).

5.  Ajoutez à vos statistiques le calcul de la durée minimale et maximale d'une réparation.

6.  Essayez de modifier votre code pour gérer un nombre variable de réparations dans le fichier texte.

7.  Créez une méthode statique trierReparations( ) qui prend en paramètre:

    a.  le tableau de réparation

    b.  une String qui peut avoir les valeurs "date" ou "duree"

    c.  un booléen indiquant croissant (si vrai) ou décroissant (si
        > faux).

> Cette méthode triera donc le tableau par date ou par duree, selon les
> valeurs des paramètres ci-dessus et retournera le tableau trié.
> (affichez-le ensuite pour vérifier le fonctionnement).

8.  Créez une interface de gestion des réparation en ligne de commande
    > en utilisant un Scanner sur System.in. L'utilisateur entre des
    > codes (à vous de les définir) qui exécutent les méthodes statiques
    > que vous jugez intéressantes.
    > Par exemple, l'affichage dans la console pourrait
    > donner:

![console 2](myMediaFolder/media/image5.png?cropResize=600,600)
-->