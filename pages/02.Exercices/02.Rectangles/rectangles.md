---
template: item
published: true
---

Exercice Rectangles

===

# Exercice &quot;Rectangles&quot;

## Objectifs

Cet exercice travaille les compétences suivantes:

- Concept de classe
- Concept d&#39;attributs
- Concept de méthode
- Concept d&#39;instance
- le mot clé &quot;new&quot;
- le mot clé &quot;this&quot;

- L&#39;utilisation du toString (implicite ou explicite)
- Les boucles et les tableau d&#39;instances

## Travail

### Partie 1

- Créer une classe java nommée Rectangle possédant les attributs largeur, hauteur, couleur ainsi que les méthodes calculerPerimetre(), calculerSurface() et estCeUnCarre(). Le constructeur doit initialiser les attributs largeur et hauteur et par défaut, la couleur est &quot;noir&quot;.

- Créer ensuite 3 instances de Rectangle aux dimensions et couleurs qui permettent de tester toutes les méthodes demandées (donc il faut au moins un carré).

### Partie 2

- Implémentez la méthode toString() dans la classe Rectangle pour qu&#39;elle affiche la couleur, le périmètre et la surface, par exemple:

```"Je suis un rectangle bleu de 40 de périmètre et de 100 de surface."```



### Partie 3

- Créer 2 instances supplémentaires (différentes)
- Ranger les 5 instances précédemment créées dans un tableau de Rectangle puis afficher sur le flux de sortie les caractéristiques de chaque instance dans une boucle en faisant appel (explicitement ou implicitement) au toString() de la classe Rectangle.

### Partie 4
- Créer une classe Dessin avec deux attributs: l'un pour représenter un nom donné au dessin et l'autre pour contenir les rectangles qui le compose (le deuxième attribut est donc du type "tableau de rectangles")
- Ajouter une méthode qui calcule le total de la surface du dessin formée par tous les rectangles
- Ajouter un toString qui retourne un résumé des rectangles (sous format String, pas de print)
- Créer un dessin et l'afficher depuis la méthode main