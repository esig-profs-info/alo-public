---
template: item
published: true
---

===

# Exercices Heritage Bases

## Objectifs:

1. Créer des classes héritant d'un classe mère, ayant des attributs / méthodes commun(e)s
1. Comprendre le mot clé _super_

## Travail
1. Créer une classe mère (de votre choix)
1. Créer deux classes enfant possédant des attributs privés
1. Créer les constructeurs partout qui initialisent au moins 1 attributs
1. Créer les toString() partout en utilisant le mot clé super
1. Créer une classe de test qui crée des objets de type classe enfant
1. Utiliser des méthodes et des attributs sur ces objets de leur classe mère et de leur propre classe
1. Créer un fichier txt contenant des données de création de vos objets (5 lignes au moins)
1. Lire le fichier txt pour créer les objets contenus