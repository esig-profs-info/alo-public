---
template: blog
content:
    items: '@self.children'
    order:
        by: folder
        dir: asc
---
# Exercices de lecture de code en Java