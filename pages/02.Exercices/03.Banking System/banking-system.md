---
template: item
published: true
---

# BankingSystem - partie 1

## Des comptes bancaires

Dans cette première phase, nous allons créer une classe Java représentant un compte bancaire.

### Création de la classe, ses attributs et un constructeur

Commencez par créer une _classe_ publique (public) nommée Account.

Cette classe a deux _attributs_ privés (private) pour le numéro de compte et le solde courant.

Nommez ces attributs `accountNumber` et `balance` (anglais pour &quot;solde&quot;) respectivement, et choisissez un type approprié, sachant qu&#39;un numéro de compte peut inclure des lettres, et que le solde est au centime près.

Ajoutez un _constructeur_ à la classe, avec un _paramètre_ pour spécifier le numéro de compte. Ce constructeur doit initialiser la valeur de l&#39;attribut balance à zéro.

---
**Astuce**
Un _constructeur_ est une méthode particulière, qui porte exactement le même nom que sa classe, et qui permet d’initialiser une nouvelle instance de cette classe.
Inspirez-vous d'une classe que vous avez déjà vu, dont les attributs et le constructeur ressemblent à ce qui vous est demandé ici !

---

Pour tester votre classe, créez une deuxième classe, nommée `AccountTest` qui sera la classe Main

Puis, vous créez une autre méthode que vous appelez `testerAffichage`.

Dans cette dernière méthode, ajoutez le code suivant :

```java
Account account1 = new Account("A1");
System.out.println("Voici mon compte: " + account1);
```

La première ligne crée un nouveau compte, c&#39;est-à-dire une nouvelle instance de la classe compte, et la deuxième l&#39;affiche.

La méthode main doit appeler la méthode `testerAffichage`.

**Exécutez ce code. Que voyez vous ?**

### Affichage du compte

Pour l&#39;instant, l&#39;affichage de notre compte n&#39;est pas parlant du tout. Pour y remédier, vous allez exploiter le mécanisme que Java utilise pour afficher n&#39;importe quel objet de n&#39;importe quelle classe. Nous verrons le fonctionnement de ce mécanisme plus tard, pour l&#39;instant, ajoutez simplement la méthode suivante à votre classe `Account` : 
```java
public String toString()
```


Remplissez le corps de la méthode pour retourner une chaîne de caractères afin que l&#39;affichage devienne (sans modifier la méthode `testerAfichage`):

```
Voici mon compte: Account<accountNumber="A1", balance=0.0>;
```

Evitez en particulier un double affichage de "Voici mon compte", celui-ci n'a rien à faire dans la méthode `toString` puisqu'il y aura des cas où l'on veut afficher plusieurs comptes, par exemple, et mettre un autre préfixe.

## Des clients

Les comptes bancaires appartiennent à des clients (c&#39;est eux qui font vivre la banque après tout). Créons-en au plus vite!

### Attributs de base

La nouvelle classe `Client` doit avoir les attributs suivants avec des noms et des types appropriés:

- Nom
- Prénom
- Identifiant numérique

### Constructeur et méthode toString

Créez un constructeur qui permet d&#39;initialiser tous ces attributs et une méthode toString pour les afficher.

### Tester

Créez une classe `ClientTest` contenant une méthode `main` et une méthode `testerAffichage` - comme pour les comptes. Vérifiez le résultat.

## Date de naissance

Nous devons également connaître la date de naissance d&#39;un client (ne serait-ce que pour savoir s&#39;il est majeur). Pour cela, ajoutez un attribut `birthdate`.

Pour représenter une date, nous allons utiliser la classe `LocalDate` qui est fournie par Java. Dans ce but, il faut tout d&#39;abord &quot;importer&quot; cette classe dans la classe Client.

```java
import java.time.LocalDate;
```


### Compléter constructeur et toString

Complétez maintenant le constructeur et la méthode toString afin d&#39;afficher également la date de naissance.

### Tester

Modifiez la méthode ClientTest.testerAffichage afin de passer une instance de LocalDate en paramètre au constructeur.

Pour passer une instance de LocalDate en paramètre il faut d&#39;abord la créer. Cela se fait à l&#39;aide de la méthode suivante:

`LocalDate.of`

Les paramètres de cette méthode sont documentés dans la [documentation Java officielle (JavaDoc)](http://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html#of-int-int-int-).

### Ajuster la mise en forme de la date

Pour changer la mise en forme de la date lors de l'affichage, vous pouvez utiliser le code ci-dessous:

```java
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

String dateMiseEnForme = formatter.format(dateNaissance);
```

Cela nécessite l&#39;import de la classe DateTimeFormatter. Son nom complet est: java.time.format.DateTimeFormatter

## Créditer et débiter un compte

Jusqu&#39;à maintenant, nous pouvons créer un compte avec un numéro donné, un solde initialisé à zéro, et l&#39;afficher. C&#39;est bien, mais pas très utile. Il faut pouvoir ajouter et retirer de l&#39;argent pour cela devienne intéressant. Mais tout d&#39;abord, nous allons ajouter une méthode qui nous permet d&#39;accéder à l&#39;attribut privé balance (le solde).

### Ajouter un _accesseur_ pour le solde

#### Code à réaliser

Ajouter une méthode dans la classe Account qui s&#39;appelle getBalance. Cette méthode doit être public et ne fait rien d&#39;autre que retourner la valeur de l&#39;attribut balance.Elle n&#39;a pas de paramètre.

#### Test à réaliser

Pour vérifier si votre méthode fonctionne comme prévue ajoutez une méthode de test dans la classer AccountTest. Cette méthode s&#39;appelle testerGetBalance. Elle crée un compte, puis affiche la valeur que retourne la méthode getBalance.

Comme suit:

```java
private static void testerGetBalance() {
    Account a = new Account("134");
    System.out.println("Après construction, le solde devrait être 0. Obtenu: " + a.getBalance());
}
```
### Rajouter du comportement : crédit

#### Code à réaliser

Ajoutez une méthode `credit` qui permet de créditer le compte d&#39;une somme donnée (ajouter au solde du compte la valeur du paramètre de la méthode).

#### Test à réaliser

Pour tester, ajouter une méthode `testerCredit` et appelez la depuis la méthode `main`. Voici le contenu de la méthode test:`

```java
    public static void testerCredit(){
    	Account account1 = new Account("A1");
    	account1.credit(50);
    	System.out.println("Après crédit de 50 le solde devrait être de 50. Obtenu: \t"+account1.getBalance());
    	account1.credit(20.5);
    	System.out.println("Après crédit de 20.5 le solde devrait être de 70.5 Obtenu: \t"+account1.getBalance());
    }
```

Vous devriez voir l&#39;affichage suivant :

```
Après crédit de 50 le solde devrait être de 50. Obtenu:        50.0
Après crédit de 50+20.5 le solde devrait être de 70.5. Obtenu:        70.5
```

**Quel est l&#39;effet du caractère « \t » ?**

### Rajouter du comportement : débit

#### Code à réaliser

Ajoutez une méthodes `debit` qui permet de débiter le compte d&#39;une somme donnée (soustraire au solde du compte la valeur du paramètre de la méthode).

#### Test à réaliser

Pour tester, ajouter une méthode `testerDebit` et appelez la depuis la méthode `main`. Voici le contenu de la méthode test:

```java
    public static void testerDebit(){
    	Account account1 = new Account("A1");
    	account1.debit(50);
    	System.out.println("Après débit de 50 le solde devrait être de -50. Obtenu: \t"+account1.getBalance());
    	account1.debit(20.5);
    	System.out.println("Après débit de 50+20.5 le solde devrait être de -70.5 Obtenu: \t"+account1.getBalance());
    }
```

Affichage attendu:

```
Après débit de 50 le solde devrait être de -50. Obtenu:        -50.0
Après débit de 50+20.5 le solde devrait être de -70.5. Obtenu:        -70.5
```

### Validation des paramètres

#### Refuser les valeurs négatives pour les paramètres

**Que se passe-t-il si vous créditez ou débitez un montant négatif ?** Modifiez votre code pour que seulement des montants positifs soient acceptés. En cas de montant négatif, les méthodes ne doivent rien faire et afficher un message d&#39;erreur.

Rajoutez des méthodes de test `testerCreditNegatif` et `testerDebitNegatif` dans la classe `AccountTest` qui vérifient que vos validations fonctionnent comme prévues.

#### Refuser les soldes négatifs

Modifiez la méthode debit pour qu&#39;elle ne modifie pas le solde si ce dernier deviendrait négatif après l&#39;opération.

Rajoutez une nouvelle méthode de test qui vérifie ce fonctionnement. Modifiez les méthodes de test déjà existantes pour qu&#39;elles fonctionnent encore (pour `testerDebit` cela peut nécessiter de faire d&#39;abord un crédit).