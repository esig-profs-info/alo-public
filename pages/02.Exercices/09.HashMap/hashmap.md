---
template: item
published: true
---

Série d'exercices sur les `HashMap`en Java.

===

## Introduction
Les dictionnaires en python sont [implémentés](https://stackoverflow.com/questions/1540673/java-equivalent-to-python-dictionaries) à l'aide d'une structure de données appelée *hashtable*.
Un équivalent existe dans tous les langages modernes, y compris en Java. 

Nous allons travailler avec la classe [HashMap](https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/util/HashMap.html).

Les méthodes les plus importantes de cette classes sont illustrées dans ce [mini tutoriel](https://www.geeksforgeeks.org/java-util-hashmap-in-java-with-examples/
).

Nous allons travailler [avec un projet fourni](HashMapMaraicher.zip) pour l'exercice 2. Vous pouvez y ajouter d'autres fichiers pour les exercices 1 et 3.

## Exercice 1

* Créer une instance de HashMap correspondant au dictionnaire python suivant: `d = {'nom': 'Dupuis', 'prenom': 'Jacque', 'age': 30}`
* Par un nouvel appel, corriger l'erreur dans le prénom, la bonne valeur est 'Jacques'.
* Afficher la liste des clés du dictionnaire (en utilisant [keySet](https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/util/HashMap.html#keySet())). Sortie attendue (l'ordre peut changer): 
* 
```
nom
prenom
age
```
* Afficher la liste des valeurs du dictionnaire (`values()`).
Sortie attendue (l'ordre peut changer):
 
```
Dupuis
Jacques
30
```
* Afficher la liste des paires clé/valeur du dictionnaire. (avec et sans utiliser la fonction `entrySet()`)
Sortie attendue (l'ordre peut changer):
 
```
nom : Dupuis
prenom : Jacques
age : 30
```
* Ecrire la phrase "Jacques Dupuis a 30 ans" (en récupérant les valeurs venues du dictionnaire).

## Exercice 2

Compléter le programme ["Maraicher facile" fourni](HashMapMaraicher.zip). Il s'agit d'un programme interactif s'exécutant dans une console.

### getMapProduits
La méthode `getMapProduits(String[] noms, double[] prix, int[] stock)`doit retourner une HashMap contenant comme clé un nom de produit et comme valeur une instance de `Produit`. 

Sortie attendue (les retours à la ligne ne sont pas attendus et ne sont là que pour vous faciliter la lecture):

```
Data dump: 
{Bananes=Produit: Bananes	Prix/quantité: 4.0 Stock:100, 
Poires=Produit: Poires	Prix/quantité: 3.0 Stock:120, 
Oranges=Produit: Oranges	Prix/quantité: 1.5 Stock:50, 
Pommes=Produit: Pommes	Prix/quantité: 2.0 Stock:290}
```

### Affichage d'un produit
Compléter la clause else correspondant à l'affichage d'un produit. Si l'utilisateur saisit un nom de produit présent dans la map, l'instance correspondante est affichée.
Sinon, un message est affiché.

Voilà un exemple d'une exécution de ce programme:

```
Bienvenue chez 'Maraicher facile'!
Saississez un nom de produit ou 'listing'.
Pour terminer le programme, entrez 'fin'.
=================================================
Bananes
Produit: Bananes	Prix/quantité: 4.0	Stock:100

Saississez un nom de produit ou 'listing'.
Pour terminer le programme, entrez 'fin'.
=================================================
schtroumpf
Désolé, nous n'avons pas trouvé ce produit :-(

Saississez un nom de produit ou 'listing'.
Pour terminer le programme, entrez 'fin'.
=================================================
fin
Au revoir!

Process finished with exit code 0
```

### Listing des produits
Compléter la clause "else if" qui s'occupe de l'affiche d'un listing. Un listing est l'ensemble des produits avec leur valeur actuellement en stock.

```
Bienvenue chez 'Maraicher facile'!
Saississez un nom de produit ou 'listing'.
Pour terminer le programme, entrez 'fin'.
=================================================
listing
Bananes: 400,00 CHF
Poires: 360,00 CHF
Oranges: 75,00 CHF
Pommes: 580,00 CHF
```

## Exercice 3
Ecrire une fonction permettant d'afficher, sous la forme d'un histogramme en étoiles, le nombre de lettres majuscules d'une chaîne entrée par l'utilisateur.

Par exemple, la chaîne `"Exo 10 : Le Premier Exemple"` contient `2` fois `"E"`, `1` fois `"L"`, et `"1"` fois `"P"`.

Vous aurez donc, en sortie, quelque chose de la forme:

```
	A: (0)
	B: (0)
	C: (0)
	D: (0)
	E: ** (2)
	F: (0)
    G: (0)
    H: (0)
    I: (0)
    J: (0)
    K: (0)
    L: * (1)
    M: (0)
    N: (0)
    O: (0)
    P: * (1)	
	etc.
```
